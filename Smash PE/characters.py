import pygame
from random import randint

class Character:
	#constructeur
	def __init__(self, name, x, y, taille, largeur, speed, nAirJumps):
		self.name = name
		self.x = x
		self.y = y
		self.taille = taille
		self.largeur = largeur
		self.speed = speed
		self.jumpingAir = False
		self.nAirJumps = nAirJumps
		self.jumpCpt = 0
		self.collisions = None
		self.v = 0 #vitesse verticale
		self.acc = 0.25
		#self.moveset = Moveset(self.name)
		self.player = None
		#couleur unique au personnage généré
		self.r = randint(0, 255)
		self.g = randint(0, 255)
		self.b = randint(0, 255)
	def getColor(self):
		return pygame.Color(self.r, self.g, self.b)
	def getRect(self):
		return pygame.Rect(self.x, self.y, self.largeur, self.taille)
	def setPlayer(self, nPlayer):
		self.player = nPlayer
	def setStage(self, activeStage):
		self.collisions = activeStage.collisionList
	def draw(self, win):
		controls = self.player.getInputs() #initialisation des commandes Xinput
		axisX = controls["x"]
		axisY = controls["y"]
		#moves gauche droite
		self.x += (self.speed)*axisX 
		if axisY > 0.5: self.acc = 0.69 #fast fall dead zone
		self.v += self.acc
		self.y += 0.5*self.acc + self.v
#GESTION COLLISIONS		
		collision = self.getRect().collidelist(self.collisions)		
		if collision != -1:
			rectCollision = self.collisions[collision]
			#arrivée de la gauche	
			if self.x + self.largeur >= rectCollision.x and self.speed*(axisX) > 0 and self.y + (self.taille//1.8) > rectCollision.y and self.v >= 0:
				print("gauche")
				self.x = rectCollision.x - self.largeur
			#arrivée du bas	
			elif self.y < rectCollision.y + rectCollision.h and (self.x + self.largeur > rectCollision.x or self.x < rectCollision.x + rectCollision.w - self.largeur) and self.v < 0:
				print("bas")
				self.y = rectCollision.y + rectCollision.h			
			#arrivée de la droite		
			elif self.x <= rectCollision.x + rectCollision.w and self.speed*(axisX) < 0 and self.y + (self.taille//1.8) > rectCollision.y:
				print("droite")
				self.x = rectCollision.x + rectCollision.w						
			#arrivée du haut	
			elif self.y + self.taille > rectCollision.y and self.v > 0:
				self.v = 0
				self.acc = 0.25
				self.y = rectCollision.y - self.taille
				self.jumpCpt = -1 #un saut de plus quand on est au sol
#MOVES
		#jump				
		if axisY < -0.33 or controls["X"] or controls["Y"]:
			if self.jumpingAir and self.jumpCpt < self.nAirJumps:
				self.acc = 0.25
				self.v = -6  #jump dead zone
				self.jumpingAir = False
				self.jumpCpt += 1
		else: self.jumpingAir = True #air jump dead zone
		#dodge
		if controls["L"] or controls["R"]:
			if axisX > -0.2 or axisX < 0.2:
				#shield
				pass
		#pour le moment, on suppose fixe la taille de la fenetre
		if self.x > 700 + 200 or self.x < -200 or self.y > 500 + 200 or self.y < -200:
			#sortie de la map
			self.y = 50
			self.x = randint(100, 250)
			self.v = -0.25
		pygame.draw.rect(win, self.getColor(), self.getRect())