import pygame
from Datas import Datas
from USB_ctrl import Controller

pygame.init()
#fenetre
fenetre = pygame.display.set_mode((700, 500))
pygame.display.set_caption("Smash PE")
#controls
player1 = Controller(0)
#player2 = Controller(1)
#player3 = Controller(2)
#stages
selectedStage = Datas.getStage("Final Destination")
#characters
fightingSprite = Datas.getFighter("Valérie Douay")
fightingSprite.setPlayer(player1)
fightingSprite.setStage(selectedStage)
#fightingSprite2 = Character("fighter", 50, 50, 40, 30, 3, crapFd, 3, player2)
#fightingSprite3 = Character("fighter", 50, 50, 40, 30, 3, crapFd, 3, player3)
pygame.display.flip()
exe = True
clock = pygame.time.Clock()
while exe:
#les premiers setings de la fenetre...pouvoir se fermer
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			exe = False
	fenetre.fill((0))
	selectedStage.draw(fenetre)
	fightingSprite.draw(fenetre)
	#fightingSprite2.draw(fenetre)
	#fightingSprite3.draw(fenetre)
	pygame.display.update()
	clock.tick(60)
pygame.quit()