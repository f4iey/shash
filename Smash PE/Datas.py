from characters import Character
from stages import Stage

roster = {}
roster["fighting sprite"] = Character("fighter", 50, 50, 40, 30, 3, 1)
roster["Valérie Douay"] = Character("Douay", 50, 50, 40, 30, 3, 3)

stageList = {}
#STAGE 1
stageList["Final Destination"] = Stage("crapFd", 1, 200)
stageList["Final Destination"].createPlatform(125, 250, "main")
stageList["Final Destination"].createPlatform(127, 150, "tiny")
#STAGE 2

class Datas:
	def getRoster():
		return roster
	def getFighter(name):
		return roster[name]
	def getStages():
		return stageList
	def getStage(name):
		return stageList[name]		
